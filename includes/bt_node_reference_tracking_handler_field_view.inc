<?php

/**
 * Field handler to present a tracking code for balance transactions to nodes.
 */
class bt_node_reference_tracking_handler_field_view extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
    $this->additional_fields['uid'] = 'uid';
    $this->additional_fields['bid'] = 'bid';
  }

  function render($values) {
    $nid = $values->{$this->aliases['nid']};
    $uid = $values->{$this->aliases['uid']};
    $bid = $values->{$this->aliases['bid']};
    $values->{$this->aliases['nid']};


    
    //crypt with our own ROT13-based "encryption"
    $crypted = transaction_code_encrypt($uid, $bid, $nid);
    
    return $crypted;
  }
}