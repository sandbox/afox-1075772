<?php


       
/**
 * Filter handler for balance item tracking code (UID+NID+BID)
 */
 
class bt_node_reference_tracking_handler_filter_view extends views_handler_filter_string {

  var $no_single = TRUE;

  function value_form(&$form, &$form_state) {
    $values = array();
    
    if($this->value){
    	/*
    	// lets split the code

      $result = db_query("SELECT * FROM {balance_items} b WHERE bid IN ("  . $split[0] . ")");
      while ($transaction = db_fetch_object($result)) {
        if ($transaction->bid) {
          $values[] = $transaction->bid;
        }
      }
  */    
	}
    
   // sort($values);
   // $default_value = implode(', ', $values);
    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Tracking code'),
      '#description' => t('Enter your tracking code.'),
    );

    if (!empty($form_state['exposed']) && !isset($form_state['input'][$this->options['expose']['identifier']])) {
      $form_state['input'][$this->options['expose']['identifier']] = $default_value;
    }
  }
  
  function value_validate(&$form, &$form_state) {
   /* $values = drupal_explode_tags($form_state['values']['options']['value']);
    $uids = $this->validate_user_strings($form['value'], $values);

    if ($uids) {
      $form_state['values']['options']['value'] = $uids;
    }*/
  }

/*
  function accept_exposed_input($input) {
    $rc = parent::accept_exposed_input($input);

    if ($rc) {
      // If we have previously validated input, override.
      if (isset($this->validated_exposed_input)) {
        $this->value = $this->validated_exposed_input;
      }
    }

    return $rc;
  }
*/


  /**
   * Validate the user string. Since this can come from either the form
   * or the exposed filter, this is abstracted out a bit so it can
   * handle the multiple input sources.
   */
/*
  function validate_user_strings(&$form, $values) {
    $uids = array();
    $placeholders = array();
    $args = array();
    $results = array();
    foreach ($values as $value) {
      if (strtolower($value) == 'anonymous') {
        $uids[] = 0;
      }
      else {
        $missing[strtolower($value)] = TRUE;
        $args[] = $value;
        $placeholders[] = "'%s'";
      }
    }

    if (!$args) {
      return $uids;
    }

    $result = db_query("SELECT * FROM {users} WHERE name IN (" . implode(', ', $placeholders) . ")", $args);
    while ($account = db_fetch_object($result)) {
      unset($missing[strtolower($account->name)]);
      $uids[] = $account->uid;
    }

    if ($missing) {
      form_error($form, format_plural(count($missing), 'Unable to find user: @users', 'Unable to find users: @users', array('@users' => implode(', ', array_keys($missing)))));
    }

    return $uids;
  }
*/

  function exposed_validate(&$form, &$form_state) {
  	if(isset($_GET['tracking_code'])){
    	$ids = transaction_code_decrypt($form_state['values']['tracking_code']);
   	 $result = db_query("SELECT * FROM {balance_items} b WHERE b.nid = %d AND b.uid = %d AND b.bid = %d", $ids);
   	 if (!$transaction = db_fetch_object($result)) form_error($form, 'Given code is not valid.');
   	 else{
    
   //  $form_state['values']['tracking_code'] = $ids['bid'];
  /*   $form_state['view']->exposed_input['tracking_code'] = $ids['bid'];
     $form['tracking_code']['#value'] = $ids['bid'];
     $form['tracking_code']['#post']['tracking_code'] = $ids['bid'];
		 $form['#post']['tracking_code'] = $ids['bid'];
		 $form['form_id']['#post']['tracking_code'] = $ids['bid'];
		 $form['form_token']['#post']['tracking_code'] = $ids['bid'];
		 $form['form_build_id']['#post']['tracking_code'] = $ids['bid'];
		 $form['submit']['#post']['tracking_code'] = $ids['bid'];
*/
//     $form_state['view']->exposed_raw_input['tracking_code'] = $ids['bid'];
  //   $form_state['input']['tracking_code'] = $ids['bid'];
  //   $form_state['clicked_button']['#post']['tracking_code'] = $ids['bid'];
  //   $this->validated_exposed_input = $ids['bid'];
  	  }
  	 }
   }
   
	function accept_exposed_input($input) {
    if (empty($this->options['exposed'])) {
      return TRUE;
    }


    if (!empty($this->options['expose']['use_operator']) && !empty($this->options['expose']['operator']) && isset($input[$this->options['expose']['operator']])) {
      $this->operator = $input[$this->options['expose']['operator']];
    }

    if (!empty($this->options['expose']['identifier'])) {
      $value = $input[$this->options['expose']['identifier']];

      // Various ways to check for the absence of optional input.
      if (!empty($this->options['expose']['optional'])) {

        if (($this->operator == 'empty' || $this->operator == 'not empty') && $value === '') {
          $value = ' ';
        }

        if ($this->operator != 'empty' && $this->operator != 'not
empty') {
          if ($value == 'All' || $value === array()) {
            return FALSE;
          }
        }

        if (!empty($this->no_single) && $value === '') {
          return FALSE;
        }
      }


      if (isset($value)) {
        $this->value = $value;
          $ids = transaction_code_decrypt($value);
	        $this->value = array($ids['bid']);
      }
      else {
        return FALSE;
      }
    }

    return TRUE;
  }
} // class close
