<?php
// $Id: bt_node_reference.views.inc
 
 /**
 * Implementation of hook_views_data_alter()
 */
 function bt_node_reference_views_data_alter(&$data) {
  $data['balance_items']['nid'] = array(
    'title' => t('Referred Node ID'),
    'help' => t('The unique identifier the referred node in balance transaction.'),
		'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
    	'base' => 'node',
    	'base field' => 'nid',
    	'label' => t('Referred Node'),
    	'handler' => 'views_handler_relationship',
    ),
  );
  
  $data['node']['nid'] = array(
    'relationship' => array(
    	'base' => 'balance_items',
    	'base field' => 'nid',
    	'label' => t('Balance items'),
    	'handler' => 'views_handler_relationship',
    ),
  );

  $data['balance_items']['tracking_code'] = array(
	  'real field' => 'bid',
    'title' => t('Tracking code'),
    'help' => t('The unique tracking code that joins the user and referred node.'),
		'field' => array(
      'handler' => 'bt_node_reference_tracking_handler_field_view',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'bt_node_reference_tracking_handler_filter_view',
    ),
  );


return $data;
}

function bt_node_reference_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'bt_node_reference') . '/includes',
    ),
    'handlers' => array(
      'bt_node_reference_tracking_handler_field_view' => array(
        'parent' => 'views_handler_field',
      ),
      'bt_node_reference_tracking_handler_filter_view' => array(
        'parent' => 'views_handler_filter_string',
      ),
    ),
  );
}